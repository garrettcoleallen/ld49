extends Area2D

var level_transition_scene = preload("res://LevelTransition.tscn")

export(PackedScene) var next_level
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_ExitLevelArea_body_entered(body):
	if body.is_in_group("player"):
		next_level()
		
func next_level():
	var ts = level_transition_scene.instance()
	ts.transition_to_level = next_level
	get_tree().root.add_child(ts)
