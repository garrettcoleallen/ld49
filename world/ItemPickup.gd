extends Node2D

export(Player.WEAPONS) var weapon_type

# Called when the node enters the scene tree for the first time.
func _ready():
	match weapon_type:
		Player.WEAPONS.Pipe:
			$WeaponSprites/PipeSprite.visible = true
		Player.WEAPONS.Pistol:
			$WeaponSprites/PistolSprite.visible = true
		Player.WEAPONS.Rifle:
			$WeaponSprites/RifleSprite.visible = true

func _on_Area2D_area_entered(area):
	if area.owner.is_in_group("player"):
		area.owner.get_weapon(weapon_type)
		if weapon_type != Player.WEAPONS.Pipe:
			GameData.alter_score(-15)
		queue_free()
