extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var health = 20

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func take_damage(dmg, stun = 0, direction = Vector2()):
	health -= dmg
	$CPUParticles2D.emitting = true
	if health < 20:
		$boards.frame = 1
		$boards2.frame = 1
	
	if health <= 0:
		$boards.frame = 2
		$boards2.frame = 2
		remove_child($StaticBody2D)
		remove_child($Area2D)
		$AudioStreamPlayer2D.play()
