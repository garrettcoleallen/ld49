extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


var is_cleared = false
func _on_HitboxArea_area_entered(area):
	if area.owner.is_in_group("player"):
		if is_cleared:
			return
		is_cleared = true
		GameData.faces_cleared += 1
		GameData.alter_score(100)
		$AnimationPlayer.play("disappear")
