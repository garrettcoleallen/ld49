extends Node2D

export(NodePath) var target =  null


var target_node = null
var target_node_parent = null
export var lever_enabled = false

func _ready():
	target_node = get_node(target)
	target_node_parent = target_node.get_parent()
	
func flip_lever():
	lever_enabled = not lever_enabled
	update_lever()
	
func update_lever():
	if lever_enabled:
		$Sprite.frame = 0
		if !(target_node_parent as Node).is_a_parent_of(target_node):
			target_node_parent.add_child(target_node)
	else:
		$Sprite.frame = 1
		if (target_node_parent as Node).is_a_parent_of(target_node):
			target_node_parent.remove_child(target_node)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_HitBox_body_entered(body):
	if body.is_in_group("player"):
		flip_lever()
