extends Game


func _ready():
	pass

func _on_InitiateFightHitbox_body_entered(body):
	if body.is_in_group("player"):
		$IntroPlayer.play("intro")


func _on_Nightmare_die():
	$TileMap/Player.hide_ui()
	$AnimationPlayer.play("end game")
