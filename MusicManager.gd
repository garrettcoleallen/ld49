extends Node


enum AUDIO_BUS {
	MUSIC = 1,
	SFX = 2
}


var music_volume = 50 setget set_music_volume
var sfx_volume = 50 setget set_sfx_volume

func get_bus_volume(bus):
	return AudioServer.get_bus_volume_db(bus) / 0.5 + 50

func _process(delta):
	if Input.is_action_just_pressed("mute_music"):
		toggle_bus_mute(AUDIO_BUS.MUSIC)
	if Input.is_action_just_pressed("mute_sfx"):
		toggle_bus_mute(AUDIO_BUS.SFX)

func toggle_bus_mute(bus):
	AudioServer.set_bus_mute(bus, not AudioServer.is_bus_mute(bus))

func set_bus_volume(bus, vol):
	vol = int(vol)
	bus = int(bus)
	AudioServer.set_bus_volume_db(bus, (vol - 50) * 0.5)

func set_sfx_volume(vol):
	sfx_volume = vol
	set_bus_volume(AUDIO_BUS.SFX, (vol - 50) * 0.5)
	
func set_music_volume(vol):
	music_volume = vol
	set_bus_volume(AUDIO_BUS.MUSIC, (vol - 50) * 0.5)
