extends Node2D


signal game_start

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var transition_in : bool = false
export(PackedScene) var transition_to_level : PackedScene


# Called when the node enters the scene tree for the first time.
func _ready():
	visible = true
	$CanvasLayer/LeftDoor.visible = true
	$CanvasLayer/RightDoor.visible = true
	if transition_in:
		get_tree().paused = true
		print("opening doors")
		$AnimationPlayer.play("open_doors")
	else: 
		get_tree().paused = true
		$AnimationPlayer.play("close_doors")

func _load_level():
	get_tree().change_scene_to(transition_to_level)
	queue_free()

func _start_level():
	get_tree().paused = false
	queue_free()
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
