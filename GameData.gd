extends Node

var player_health = 100
var player_weapons = []
var player_score = 0
var player_current_weapon = 0
var health_packs = 0
var score = 0
var easy_mode = false

var faces_cleared = 0
var faces_spawned = 5

var metaData = {}

var has_saved = false






var level_start_player_health = player_health
var level_start_player_weapons = player_weapons
var level_start_player_score = player_score
var level_start_player_current_weapon = player_current_weapon
var level_start_health_packs = health_packs
var level_start_score = score
var level_start_faces_cleared = faces_cleared
func record_level_start():
	level_start_player_health = player_health
	level_start_player_weapons = player_weapons
	level_start_player_score = player_score
	level_start_player_current_weapon = player_current_weapon
	level_start_health_packs = health_packs
	level_start_score = score
	level_start_faces_cleared = faces_cleared
	
func reset_level_start():
	player_health = level_start_player_health
	player_weapons = level_start_player_weapons
	player_score = level_start_player_score
	player_current_weapon = level_start_player_current_weapon
	health_packs = level_start_health_packs
	score = level_start_score
	faces_cleared = level_start_faces_cleared

func reset():
	faces_cleared = 0
	has_saved = false
	player_score = 0
	health_packs = 0
	player_current_weapon = 0
	player_weapons = []
	player_health = 100
	metaData = {
		"faces_cleared": 0
	}

func alter_score(val):
	if !easy_mode:
		score += val

func _ready():
	reset()
