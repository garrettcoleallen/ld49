class_name Game
extends Node2D

var level_transition_scene = preload("res://LevelTransition.tscn")

export var reset_progress = false

var original_tiles = []

# Called when the node enters the scene tree for the first time.
func _ready():
	if reset_progress:
		GameData.has_saved = false
		GameData.reset()
	$InsanityTileMap.visible = false
	# record original tiles on the map for later
	for tile in $InsanityTileMap.get_used_cells():
		original_tiles.append({
			"coords": tile,
			"flip_h": $TileMap.is_cell_x_flipped(tile.x, tile.y),
			"id": $TileMap.get_cellv(tile)
		})
	$BGMusic/BGMusicPlayer.play("fade-in")
	$TileMap/Player.update_insanity()


func _on_Player_insanity_change(insanity):
	if insanity:
		$BGMusic/BGMusicDistort.play("distort")
		for tile in $InsanityTileMap.get_used_cells():
			$TileMap.set_cellv(tile, $InsanityTileMap.get_cellv(tile), $InsanityTileMap.is_cell_x_flipped(tile.x, tile.y))
	else:
		$BGMusic/BGMusicDistort.play("normal")
		for tile in original_tiles:
			$TileMap.set_cellv(tile.coords, tile.id, tile.flip_h)
	
	update_objects_for_insanity(insanity)


var insanity_objects = []
var sanity_objects = []
func update_objects_for_insanity(insanity):
	if insanity:
		for node in get_tree().get_nodes_in_group("sanity-object"):
			sanity_objects.append(node)
			node.get_parent().remove_child(node)
		for node in insanity_objects:
			$TileMap.add_child(node)
		insanity_objects = []
	else:
		for node in get_tree().get_nodes_in_group("insanity-object"):
			insanity_objects.append(node)
			node.get_parent().remove_child(node)
		for node in sanity_objects:
			$TileMap.add_child(node)
		sanity_objects = []
			

func _on_Player_restart_level():
	get_tree().change_scene(get_tree().current_scene.filename)
	
#	if get_tree().current_scene.filename == "res://Game.tscn":
#		get_tree().change_scene("res://Game.tscn")
#	else:
#		var ts = level_transition_scene.instance()
#		ts.transition_to_level = get_tree().current_scene.filename
#		get_tree().root.add_child(ts)
