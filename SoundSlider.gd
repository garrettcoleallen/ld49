extends HSlider


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export(MusicManager.AUDIO_BUS) var bus

# Called when the node enters the scene tree for the first time.
func _ready():
	value = MusicManager.get_bus_volume(bus)


func _on_SoundSlider_value_changed(value):
	print("change to ", value)
	MusicManager.set_bus_volume(bus, value)


func _on_CheckBox_pressed():
	MusicManager.toggle_bus_mute(bus)
	$CheckBox.pressed = AudioServer.is_bus_mute(bus)
