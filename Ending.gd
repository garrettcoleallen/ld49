extends Node2D


var lb_s = preload("res://leaderboard/Leaderboard.tscn")
# Called when the node enters the scene tree for the first time.
func _ready():
	if GameData.faces_cleared >= GameData.faces_spawned:
		$AnimationPlayer.play("Ending2")
		GameData.score += 300
	else:
		$AnimationPlayer.play("Ending1")


func move_to_lb():
	$BadEnding.visible = false
	$GoodEnding.visible = false
	var lb = lb_s.instance()
	lb.show_submit_score = true
	lb.submit_score_value = GameData.score
	lb.submit_score_meta = GameData.metaData
	add_child(lb)
