extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
		
	var width = get_viewport_rect().end.x
	var height = get_viewport_rect().end.y
	
	$Background.scale.x = width/1920
	$Background.scale.y = height/1080
	
	$Title.position.x = width/2
	$Title.position.y = (height/2) - 40
	$Title.scale.x = width/1920
	$Title.scale.y = $Title.scale.x
	
	print(Version.get_version())

func _process(delta):
	if Input.is_action_just_pressed("ui_accept"):
		_transition_to_game()
	
func _play_jam():
	$JamSeriesPlayer.play("jam")


var is_transitioning = false
func _transition_to_game():
	if !is_transitioning:
		is_transitioning = true
		get_tree().change_scene("res://Menu.tscn")
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass



func _on_GMGTitleVideo_finished():
	
	_play_jam()


func _on_Button_pressed():
	_transition_to_game()
