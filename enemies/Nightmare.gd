extends KinematicBody2D

signal die
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

enum MOVES {
	Melee,
	Charge,
	Summon,
	Consume,
	CauseInsanity,
	InsanityHop #go to insanity only mode	
}

var spawner_scene = preload("res://enemies/Spawner.tscn")

export var can_move = true
export var active = false setget set_active
export var phase = 1

export var damage_resistance = 0.25

export var stun_resistance = 75



var phase_moves = [
	[ # phase 1
		MOVES.Melee, # follows player, in range does instant kill grab move
		MOVES.Summon # summons minion :( idk something cool animation wise i think
	],
	[ # phase 2
		MOVES.Charge,
		#MOVES.CauseInsanity # grab head and cause player to go insane for 15 seconds
	],
	[ # phase 3
		MOVES.Consume, # appears at a minion and does an eat animation
		MOVES.InsanityHop # go to face mode, re-appears if player goes insane
	]
]

var phase_health = [
	500
]

var available_moves = []

var dead = false
var stun_acc = 0
var health = 500

var current_move = null


var rng
# Called when the node enters the scene tree for the first time.
func _ready():
	rng = RandomNumberGenerator.new()
	rng.randomize()
	$CanvasLayer/HealthBar.max_value = health
	$CanvasLayer/HealthBar.value = health


func _physics_process(delta):
	if !active or dead:
		return
	match current_move:
		MOVES.Melee:
			_move_melee(delta)
		MOVES.Summon:
			_move_summon(delta)
		MOVES.Charge:
			_move_charge(delta)
	

var charge_destination = null
var charge_speed = 400
var charges = 0
var max_charges =3
func _move_charge(delta):
	$AnimationPlayer.play("charge")
	if _is_player_in_melee_range():
		_get_player().take_damage(100)
	if charge_destination == null && $ChargeTimer.is_stopped():
		var dests = get_tree().get_nodes_in_group("charge_point")
		charge_destination = dests[rng.randi() % dests.size()]
		
	if charge_destination != null:
		var direction = global_position.direction_to(charge_destination.global_position)
		if direction.x < 0:
			$MeleeDamageArea.scale.x = abs($MeleeDamageArea.scale.x)
			$MeleeDetectArea.scale.x = abs($MeleeDetectArea.scale.x)
			$Sprite.scale.x = abs($Sprite.scale.x)
		else:
			$MeleeDamageArea.scale.x = -1 * abs($MeleeDamageArea.scale.x)
			$MeleeDetectArea.scale.x = -1 * abs($MeleeDetectArea.scale.x)
			$Sprite.scale.x = -1 * abs($Sprite.scale.x)
			
		global_position += delta * direction * charge_speed
		if delta * charge_speed > abs(global_position.distance_to(charge_destination.global_position)):
			charges += 1
			if charges >= max_charges:
				charges = 0
				_end_move()
			charge_destination = null
			$ChargeTimer.start()
		

func _move_melee(delta):
	if $MeleeTimer.is_stopped() && !dead:
		$MeleeTimer.start()
	if can_move:
		var player = _get_player()
		if player:
			var direction = global_position.direction_to(player.global_position)
			
			$AnimationPlayer.play("walk")
			move_and_slide(direction.normalized() * 200)
			if direction.x < 0:
				$MeleeDamageArea.scale.x = abs($MeleeDamageArea.scale.x)
				$MeleeDetectArea.scale.x = abs($MeleeDetectArea.scale.x)
				$Sprite.scale.x = abs($Sprite.scale.x)
			else:
				$MeleeDamageArea.scale.x = -1 * abs($MeleeDamageArea.scale.x)
				$MeleeDetectArea.scale.x = -1 * abs($MeleeDetectArea.scale.x)
				$Sprite.scale.x = -1 * abs($Sprite.scale.x)
			_check_can_attack_player()
	
func _get_player() -> Player:
	var player = get_tree().get_nodes_in_group("player").front()
	if player && !player.dead:
		return player
	return null

var max_spawns = 3
var current_spawns = 0
func _move_summon(delta):
	$AnimationPlayer.play("summon")
	if current_spawns >= max_spawns:
		current_spawns = 0
		_end_move()
	elif $SpawnerTimer.is_stopped():
		# spawn dudes
		var spawn_locations = get_tree().get_nodes_in_group("spawner_location")
		var spawn_location = spawn_locations[rng.randi() % spawn_locations.size()]
		var s = spawner_scene.instance()
		get_tree().get_nodes_in_group("map").front().add_child(s)
		s.global_position = spawn_location.global_position
		current_spawns += 1
		$SpawnerTimer.start()
		pass
		
func _stunned():
	stun_acc = 0
	current_move = null

func _end_move():
	$AnimationPlayer.play("idle")
	$MoveCooldown.start()
	current_move = null

func set_active(val):
	active = val
	$CanvasLayer/HealthBar.visible = active
	if active:
		update_available_moves()
		begin_new_move()

func update_available_moves():
	available_moves = []
	for i in range(0, phase):
		for move in phase_moves[i]:
			available_moves.append(move)

		

func begin_new_move():
	var random_move = available_moves[rng.randi() % available_moves.size()]
	current_move = random_move

func _on_MoveCooldown_timeout():
	begin_new_move()
	
func _on_MeleeTimer_timeout():
	_end_move()
	
func attempt_player_damage():
	for area in $MeleeDamageArea.get_overlapping_areas():
		if area.owner.is_in_group("player"):
			area.owner.take_damage(50)


func _is_player_in_melee_range() -> bool:
	for area in $MeleeDetectArea.get_overlapping_areas():
		if area.owner.is_in_group("player") && !area.owner.dead:
			return true
	return false
	
func _check_can_attack_player():
	if _is_player_in_melee_range():
		$AnimationPlayer.play("melee")

func _on_MeleeDetectArea_area_entered(area):
	if current_move != MOVES.Melee:
		return
	if dead:
		return
	if area.owner.is_in_group("player") && !area.owner.dead:
		print("playing animation attack")
		$AnimationPlayer.play("melee")
		
func end_phase():
	if phase == 2:
		die()
	else:
		$PlayerInsanityTimer.start()
		phase += 1
		health = phase_health[phase-2]
		$CanvasLayer/HealthBar.max_value = health
		$CanvasLayer/HealthBar.value = health
		update_available_moves()
		_end_move()
		
func die():
	$CanvasLayer/HealthBar.visible = false
	$CanvasLayer/HealthBar/Label.visible = false
	current_move = null
	dead = true
	can_move = false
	$HitBox.monitorable = false
	$AnimationPlayer.play("death")
	GameData.alter_score(200)
	

func take_damage(dmg, stun = 0, direction = Vector2()):
	if dead or !active:
		return
	stun_acc += stun
	health -= dmg
	$CanvasLayer/HealthBar.value = health
	if health <= 0:
#		$Hitbox.visible = false
#		$Attack1InitiateArea.monitoring = false
#		$Attack1DamageArea.monitoring = false
#		$CollisionShape2D.disabled = true
#
#		dead = true
#		can_move = false
		
		end_phase()
	elif stun_acc >= stun_resistance:
		stun_acc = 0
		_stunned()
		$AnimationPlayer.play("stunned")


func _on_PlayerInsanityTimer_timeout():
	var player = _get_player()
	if player:
		if player.insanity_override != null:
			player.override_insanity(null)	
		else:
			player.override_insanity(not player.is_insane())
