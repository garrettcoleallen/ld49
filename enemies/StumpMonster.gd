extends KinematicBody2D

var projectile_scene = preload("res://enemies/StumpProjectile.tscn")

const RUN_SPEED = 375
const STUN_RESISTANCE = 15

export var can_move = true
export var activation_distance = 650

export var ranged_distance = 650

var melee_damage = 35
var melee_stun = 25

var health = 150
var stun_acc = 20
var dead = false
var activated = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	#z_index = global_position.y
	pass

var player_throw_at_location = null
func _physics_process(delta):
	if can_move && !dead:
		var player = _get_player()
		if player:
			var direction = global_position.direction_to(player.global_position)
			var player_dist = abs(global_position.distance_to(player.global_position))
			if (player_dist < activation_distance or activation_distance < 0) && !activated:
				activated = true
				$ActivationSoundPlayer.play()
			if !activated:
				return
			
			if direction.x < 0:
				$MeleeHitArea.scale.x = abs($MeleeHitArea.scale.x)
				$stump.scale.x = abs($stump.scale.x)
			else:
				$MeleeHitArea.scale.x = -1 * abs($MeleeHitArea.scale.x)
				$stump.scale.x = -1 * abs($stump.scale.x)
				
			if !_check_can_melee_player():
				if player_dist < ranged_distance:
					player_throw_at_location = player.global_position
					$AnimationPlayer.play("ranged_attack")
				else:
					$AnimationPlayer.play("idle")
				
				

func take_damage(dmg, stun = 0, direction = Vector2()):
	activated = true # in case they arent yet
	if dead:
		return
		
	if $AnimationPlayer.current_animation == "stun":
		dmg *= 2
		
	stun_acc += stun
	health -= dmg
	if health <= 0:
		$Hitbox.visible = false
		$CollisionShape2D.disabled = true
		GameData.alter_score(5)
		dead = true
		can_move = false
		$AnimationPlayer.play("death")
	elif stun_acc >= STUN_RESISTANCE:
		stun_acc = 0
		can_move = false
		$AnimationPlayer.play("stun")

func _get_player() -> Player:
	var player = get_tree().get_nodes_in_group("player").front()
	if player && !player.dead:
		return player
	return null


#func attempt_player_damage():
#	for area in $Attack1DamageArea.get_overlapping_areas():
#		if area.owner.is_in_group("player"):
#			area.owner.take_damage(damage)

#
#func _on_Attack1InitiateArea_area_entered(area):
#	if area.owner.is_in_group("player") && !area.owner.dead:
#		can_move = false
#		$AnimationPlayer.play("attack")

func _spawn_projectile():
	var projectile = projectile_scene.instance()
	projectile.global_position = $stump/ProjectileSpawn.global_position
	var player = _get_player()
	if player:
		projectile.look_at(player.global_position)
		projectile.direction = $stump/ProjectileSpawn.global_position.direction_to(player_throw_at_location)
	get_tree().root.add_child(projectile)


func _check_can_melee_player() -> bool:
	for area in $MeleeHitArea.get_overlapping_areas():
		if area.owner.is_in_group("player") && !area.owner.dead && !$AnimationPlayer.current_animation == "melee_attack":
			can_move = false
			$AnimationPlayer.play("melee_attack")
			return true
	return false
	
func _deal_melee_damage():
	for area in $MeleeHitArea.get_overlapping_areas():
		if area.owner.is_in_group("player") && !area.owner.dead:
			area.owner.take_damage(melee_damage, melee_stun)
	can_move = true

func _spawn_body():
	var body = $stump.duplicate(true)
	body.global_position = global_position + $stump.position
	get_tree().root.add_child(body)
	queue_free()
	
