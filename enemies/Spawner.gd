extends Node2D


enum MONSTERS {
	Naked,
	Stumpy
}

var monster_scenes = {
	MONSTERS.Naked: preload("res://enemies/RunnerMonster.tscn"),
	MONSTERS.Stumpy: preload("res://enemies/StumpMonster.tscn"),
}

export(MONSTERS) var monster_type


# Called when the node enters the scene tree for the first time.
func _ready():
	if !monster_type:
		randomize()	
		monster_type = monster_scenes.keys()[randi() % monster_scenes.keys().size()]
	
	
func spawn_monster():
	var node = monster_scenes[monster_type].instance()
	get_tree().get_nodes_in_group("map").front().add_child(node)
	node.global_position = global_position
	node.activation_distance = -1
	
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
