extends KinematicBody2D


const RUN_SPEED = 375
const STUN_RESISTANCE = 50

export var can_move = true
export var activation_distance = 300

var damage = 25

var health = 30
var stun_acc = 0
var dead = false
var activated = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	#z_index = global_position.y
	pass

func _physics_process(delta):
	if can_move:
		var player = _get_player()
		if player:
			var direction = global_position.direction_to(player.global_position)
			if (global_position.distance_to(player.global_position) < activation_distance or activation_distance < 0) && !activated:
				$ActivationSoundPlayer.play()
				activated = true
			if !activated:
				return
			$AnimationPlayer.play("run")
			move_and_slide(direction.normalized() * RUN_SPEED)
			if direction.x < 0:
				$Attack1DamageArea.position.x = -23
				$Attack1InitiateArea.scale.x = -1 * abs($Attack1InitiateArea.scale.x)
				$naked.scale.x = -1 * abs($naked.scale.x)
				$corruption.scale.x = -1 * abs($corruption.scale.x)
			else:
				$Attack1DamageArea.position.x = 23
				$Attack1InitiateArea.scale.x = abs($Attack1InitiateArea.scale.x)
				$naked.scale.x = abs($naked.scale.x)
				$corruption.scale.x = abs($corruption.scale.x)
			_check_can_attack_player()
				

func take_damage(dmg, stun = 0, direction = Vector2()):
	activated = true # in case they arent yet
	if dead:
		return
	stun_acc += stun
	health -= dmg
	if health <= 0:
		$Hitbox.visible = false
		$Attack1InitiateArea.monitoring = false
		$Attack1DamageArea.monitoring = false
		$CollisionShape2D.disabled = true
		GameData.alter_score(5)
		
		dead = true
		can_move = false
		$AnimationPlayer.play("death")
	elif stun_acc >= STUN_RESISTANCE:
		stun_acc = 0
		can_move = false
		move_and_slide(direction*200)
		$AnimationPlayer.play("stun")

func _get_player() -> Player:
	var player = get_tree().get_nodes_in_group("player").front()
	if player && !player.dead:
		return player
	return null


func attempt_player_damage():
	for area in $Attack1DamageArea.get_overlapping_areas():
		if area.owner.is_in_group("player"):
			area.owner.take_damage(damage)


func _on_Attack1InitiateArea_area_entered(area):
	if area.owner.is_in_group("player") && !area.owner.dead:
		can_move = false
		$AnimationPlayer.play("attack")


func _check_can_attack_player():
	for area in $Attack1DamageArea.get_overlapping_areas():
		if area.owner.is_in_group("player") && !area.owner.dead:
			$AnimationPlayer.play("attack")
			return
	can_move = true
	
func _spawn_body():
	var body = $naked.duplicate(true)
	body.global_position = global_position + $naked.position
	get_tree().root.add_child(body)
	
