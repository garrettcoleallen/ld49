extends Node2D

var ooze_s = preload("res://enemies/StumpOoze.tscn")


export var damage = 30
export var stun = 50
export var direction = Vector2()


export var speed = 10

func _physics_process(delta):
	global_position += direction.normalized()*speed

var contact = false
func _on_HitBox_area_shape_entered(area_id, area, area_shape, local_shape):
	if contact:
		return
	if !area.owner.is_in_group("monster"):
		if area.owner.has_method("take_damage"):
			area.owner.take_damage(damage, stun, direction)
		speed = 0
		contact = true
		$AnimationPlayer.play("contact")

		
func _spawn_ooze():
	var o = ooze_s.instance()
	o.global_position = global_position + Vector2(0, 16)
	get_tree().get_nodes_in_group("map").front().add_child(o)
	o.global_position = global_position + Vector2(0, 16)
	queue_free()


func _on_Timer_timeout():
	queue_free()


func _on_HitBox_body_entered(body):
	if contact:
		return
	if !body.is_in_group("monster"):
		if body.has_method("take_damage"):
			body.take_damage(damage, stun, direction)
		speed = 0
		contact = true
		$AnimationPlayer.play("contact")
