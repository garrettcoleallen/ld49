extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func get_version() -> String:
	return "v1.0.4"
#	return _read_version_file()

#func _read_version_file():
#	var path_to_version = "res://gitversion.gd"
#	var file = File.new()
#	if file.file_exists(path_to_version):
#		file.open(path_to_version, File.READ)
#		var content = file.get_as_text()
#		file.close()
#		return content
#	return "-.-.-"
