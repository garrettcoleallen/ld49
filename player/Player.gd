class_name Player
extends KinematicBody2D


signal insanity_change
signal restart_level

var pipe_slash_scene = preload("res://player/weapons/PipeSlashProjectile.tscn")
var pistol_bullet_scene = preload("res://player/weapons/PistolProjectile.tscn")

const MAX_HEALTH = 100
const STUN_RESISTANCE = 30
const INSANITY_HEALTH = 25
const MAX_HEALTH_PACKS = 3

const HEALTH_PACK_VALUE = 20

enum WEAPONS {
	None,
	Pipe,
	Pistol,
	Shotgun,
	Rifle
}


export var point_arm_at_cursor = false
export var slow_movement = false


export var can_move = true
var dead = false
var health = 100
var stunc_acc = 0
var health_packs = 0 setget set_health_packs

var insanity_transition_location = Vector2()
var insanity_override = null setget override_insanity

var weapons = [WEAPONS.None]

var current_weapon_idx = 0
func next_weapon():
	if slow_movement:
		return
	current_weapon_idx += 1
	if current_weapon_idx >= len(weapons):
		current_weapon_idx = 0
	_update_weapon_sprite()
	
func hide_ui():
	$UILayer/HealthBar.visible = false
	$UILayer/HealthBar/VersionText.visible = false
	$UILayer/HealthBar/InsanityOverride.visible = false
	$UILayer/ShadowOverlay.visible = false
	$UILayer/InsanityBorder.visible = false

func override_insanity(val):
	$UILayer/HealthBar/InsanityOverride.visible = val == true
	insanity_override = val
	update_insanity()

func get_weapon(weapon):
	if !weapons.has(weapon):
		weapons.append(weapon)
		current_weapon_idx = len(weapons)-1
		_update_weapon_sprite()

func take_damage(dmg, stun = 0, direction = Vector2()):
	if dead:
		return
	
	if GameData.easy_mode && $BodyAnimationPlayer.current_animation != "cut":
		dmg = dmg/5
	
	GameData.alter_score(-3)
	$UILayer/HurtAnimationPlayer.stop(true)	
	$UILayer/HurtAnimationPlayer.play("hurt")
	
		
	health -= dmg
	stunc_acc += stun
	
	update_insanity()
		
	if health <= 0:
		dead = true
		$BodyAnimationPlayer.play("death")
		set_deferred("monitorable", false)
	elif stunc_acc >= STUN_RESISTANCE:
		stunc_acc = 0
		print("stunned")
		
func heal(amt):
	$PlayerHealSound.play()
	health += amt
	update_insanity()
	
func pickup_health() -> bool:
	if health_packs < MAX_HEALTH_PACKS:
		$PickupHealthSound.play()
		self.health_packs += 1
		return true
	return false
	
#func set_can_move(val):
#	can_move = val
#	if !can_move && !dead:
#		$BodyAnimationPlayer.play("idle")
	
func update_insanity():
	var val = is_insane()
	
	emit_signal("insanity_change", val)
	
	$UILayer/InsanityParticles.emitting = val
	if val:
		$UILayer/InsanityBorder.visible = true
		$UILayer/InsanityIntroPlayer.play("insanity")
	else:
		$UILayer/InsanityBorder.visible = false
		
	_validate_position_still_valid()
	insanity_transition_location = global_position
	
#make sure player new  location is valid after insanity change
func _validate_position_still_valid():
	var map = get_tree().get_nodes_in_group("map").front() #lol	
	var local_position = map.to_local(global_position)
	var map_position = map.world_to_map(local_position)
	if map.get_cellv(map_position) < 0:
		global_position = insanity_transition_location

	
func is_insane() -> bool:
	if insanity_override != null:
		return insanity_override
	return health <= INSANITY_HEALTH
	
func make_camera_main():
	$Camera2D.current = true
	
func set_health_packs(val):
	health_packs = val
	
	$UILayer/HealthPacks.visible = health_packs != 0
	
	match health_packs:
		1:
			$UILayer/HealthPacks.text = "one healthpack"
		2:
			$UILayer/HealthPacks.text = "two healthpacks"
		3:
			$UILayer/HealthPacks.text = "three healthpacks"
	
func use_health_pack():
	heal(HEALTH_PACK_VALUE)
	self.health_packs -= 1

# Called when the node enters the scene tree for the first time.
func _ready():
	GameData.record_level_start()
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	$UILayer/Score/Value.text = str(GameData.score)
	if GameData.has_saved:
		if GameData.player_health < INSANITY_HEALTH:
			self.health = INSANITY_HEALTH + 1
		else:		
			self.health = GameData.player_health
		self.health_packs = GameData.health_packs
		self.weapons = GameData.player_weapons
		self.current_weapon_idx = GameData.player_current_weapon
		
	$UILayer/HealthBar.value = health
	insanity_transition_location = global_position
		
	next_weapon()
	$UILayer/HealthBar/VersionText.text = Version.get_version()
	
	update_insanity()
	

func _process(delta):
	$Cursor.global_position = get_global_mouse_position()
	$UILayer/Score/Value.text = str(GameData.score)
	
	# Setting health bar on screen
	var ui_rate = 100
	if abs($UILayer/HealthBar.value - health) < ui_rate*delta:
		$UILayer/HealthBar.value = health
	elif $UILayer/HealthBar.value < health:
		$UILayer/HealthBar.value += ui_rate*delta
	elif $UILayer/HealthBar.value > health:
		$UILayer/HealthBar.value -= ui_rate*delta
	
func play_body_animation(anim):
	$BodyAnimationPlayer.play(anim)

func _physics_process(delta):
	if dead or !can_move:
		return
	_player_movement(delta)
	_player_sprite(delta)
	if Input.is_action_just_pressed("switch_weapons"):
		next_weapon()
	

func _update_weapon_sprite():
	var current_weapon = weapons[current_weapon_idx]
	match current_weapon:
		WEAPONS.None:
			$ArmsAnimationPlayer.play("unarmed")
		WEAPONS.Pipe:
			$ArmsAnimationPlayer.play("pipe")
		WEAPONS.Pistol:
			$ArmsAnimationPlayer.play("pistol")
		WEAPONS.Rifle:
			$ArmsAnimationPlayer.play("rifle")
			
func _play_weapon_attack():
	var current_weapon = weapons[current_weapon_idx]
	match current_weapon:
		WEAPONS.Pipe:
			$ArmsAnimationPlayer.play("pipe attack")
		WEAPONS.Pistol:
			$ArmsAnimationPlayer.play("pistol attack")
		WEAPONS.Rifle:
			$ArmsAnimationPlayer.play("rifle attack")
	
var inverse_sprite = false
func _player_sprite(delta):
	if get_global_mouse_position().x < global_position.x && !inverse_sprite:
		$PlayerSprite.scale.x = -$PlayerSprite.scale.x
		inverse_sprite = true
	elif get_global_mouse_position().x >= global_position.x && inverse_sprite:
		$PlayerSprite.scale.x = -$PlayerSprite.scale.x
		inverse_sprite = false

func _player_movement(delta):
	var input_vector = Vector2()
	if Input.is_action_pressed("ui_up"):
		input_vector.y += -1
	if Input.is_action_pressed("ui_down"):
		input_vector.y += 1
	if Input.is_action_pressed("ui_left"):
		input_vector.x += -1
	if Input.is_action_pressed("ui_right"):
		input_vector.x += 1
	
	if Input.is_action_pressed("attack"):
		_play_weapon_attack()
		
	if Input.is_action_just_pressed("sanity_reduce"):
		$BodyAnimationPlayer.play("cut")
		return
		
	if Input.is_action_just_released("heal") && health_packs > 0:
		use_health_pack()
		return
		
	if input_vector.length() > 0:
		if ((input_vector.x > 0 && $PlayerSprite.scale.x < 0) or 
			(input_vector.x < 0 && $PlayerSprite.scale.x > 0)):
			$BodyAnimationPlayer.play_backwards("run")
		else:
			$BodyAnimationPlayer.play("run")
		
	else:
		$BodyAnimationPlayer.play("idle")
	
	if point_arm_at_cursor:
		$PlayerSprite/Arms.look_at(get_global_mouse_position())
	else:
		$PlayerSprite/Arms.rotation = 0
	
	var movement_speed = 330
	if $ArmsAnimationPlayer.current_animation == "unarmed":
		movement_speed *= 1.2
	if slow_movement:
		movement_speed /= 2
	if !$SlowDebuffTimer.is_stopped():
		movement_speed /= 2
	move_and_slide(input_vector.normalized() * movement_speed)

func slow_effect():
	$SlowDebuffTimer.start()

func _spawn_projectile():
	var current_weapon = weapons[current_weapon_idx]
	var projectile = null
	match current_weapon:
		WEAPONS.None:
			print("unarmed?")
		WEAPONS.Pipe:
			projectile = pipe_slash_scene.instance()
			if is_insane():
				projectile.damage *= 2
		WEAPONS.Pistol:
			projectile = pistol_bullet_scene.instance()
			if is_insane():
				projectile.damage *= 1.5
			projectile.stun = 4
		WEAPONS.Rifle:
			projectile = pistol_bullet_scene.instance()
			projectile.damage = 8 #rifle adjustment
			
	if projectile:
		projectile.global_position = $PlayerSprite/Arms/Weapon/WeaponProjectileSpawn.global_position
		projectile.global_rotation = $PlayerSprite/Arms/Weapon/WeaponProjectileSpawn.global_rotation
		projectile.direction = $PlayerSprite/Arms/Weapon/WeaponProjectileSpawn.global_position.direction_to(get_global_mouse_position())
		get_tree().root.add_child(projectile)
	


func _on_Player_tree_exiting():
	if !dead:
		GameData.player_health = health
		GameData.player_weapons = weapons
		GameData.player_current_weapon = current_weapon_idx
		GameData.health_packs = health_packs
		GameData.has_saved = true
		GameData.metaData['faces_cleared'] = GameData.faces_cleared
	
func _restart_level():
	GameData.reset_level_start()
	GameData.alter_score(-100)
	emit_signal("restart_level")


func _on_LocationValidator_timeout():
	_validate_position_still_valid()
