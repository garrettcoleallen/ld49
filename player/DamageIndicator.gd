extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var damage = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	$Node2D/Label.text = str(round(damage))


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
