extends Node2D

var bs_scene = preload("res://world/BloodSplat.tscn")
var di_scene = preload("res://player/DamageIndicator.tscn")

var speed = 30

export var damage = 15
var stun = 1
var direction = Vector2()

func _physics_process(delta):
	global_position += direction.normalized()*speed

var contact = false
func _on_HitBox_area_entered(area):
	if contact:
		return
	if area.owner.has_method("take_damage") && !area.owner.is_in_group("player"):
		
		if area.owner.is_in_group("bleed"):
			var bs = bs_scene.instance()
			get_tree().root.add_child(bs)
			if direction.x >= 0:
				bs.flip_h = true
			bs.global_position = global_position
				
		contact = true
		
		if "damage_resistance" in area.owner:
				damage *= area.owner.damage_resistance
				
		area.owner.take_damage(damage, stun, direction)
		speed = 0
		$AnimationPlayer.play("contact")
		var di = di_scene.instance()
		di.damage = damage
		var map = get_tree().get_nodes_in_group("map").front()
		if map:
			map.add_child(di)
			di.global_position = global_position
			di.global_position.x = area.global_position.x
		
