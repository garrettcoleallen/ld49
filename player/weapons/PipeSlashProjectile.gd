extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var bs_scene = preload("res://world/BloodSplat.tscn")
var di_scene = preload("res://player/DamageIndicator.tscn")

export var damage = 5
var stun = 20
var direction = Vector2()


func _deal_damage():
	for area in $HitBox.get_overlapping_areas():
		if area.owner.has_method("take_damage") && !area.owner.is_in_group("player"):
			
			if area.owner.is_in_group("bleed"):
				var bs = bs_scene.instance()
				get_tree().root.add_child(bs)
				if direction.x >= 0:
					bs.flip_h = true
				bs.global_position = global_position
			
			if "damage_resistance" in area.owner:
				damage *= area.owner.damage_resistance
				
			area.owner.take_damage(damage, stun, direction)
			var di = di_scene.instance()
			di.damage = damage
			var map = get_tree().get_nodes_in_group("map").front()
			if map:
				map.add_child(di)
				di.global_position = global_position
				di.global_position.x = area.global_position.x
