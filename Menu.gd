extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	$EasyMode.pressed = GameData.easy_mode


func _on_PlayButton_pressed():
	GameData.has_saved = false
	GameData.score = 0
	GameData.reset()
	get_tree().change_scene("res://Game.tscn")


func _on_LeaderboardButton_pressed():
	get_tree().change_scene("res://leaderboard/Leaderboard.tscn")


func _on_EasyMode_pressed():
	GameData.easy_mode = not GameData.easy_mode
	$EasyMode.pressed = GameData.easy_mode
